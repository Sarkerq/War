#include<iostream>
#include <queue>
#include "karta.h"
using namespace std;
const char * wart[]= {"Dwojka","Trojka","Czworka","Piatka","Szostka","Siodemka","Osemka","Dziewiatka","Dziesiatka","Walet","Dama","Krol","As"};
const char * kol[] ={"kier", "karo", "pik","trefl"};
Karta::Karta(int t, int i,int n, bool s)
{

    talia=t;
    id=i;
    nalezydo=n;
    stan=s;
}
void Karta::nazwij(int w, int k, int t, int i)
{
    wartosc= wart[w];
    starsza=w;
    kolor=kol[k];
    talia=t;
    id=i;

}
void Karta::powiedz()
{
    cout<<wartosc<<" "<<kolor<<endl;
}
int Karta::star()
{
    return starsza;
}
 bool Karta::czyrozd()
{
    if(nalezydo==10)return false;
    else return true;
}
Gracz::Gracz(string n, int p)
{
    nazwa=n;
    punkty=p;
}
void Gracz::nazwij(string n, int i)
{
    id=i;
    nazwa=n;
}
bool Gracz::czygra()
{
    if(reka.empty()) return false;
    else return true;
}

void Gracz::wolaj()
{
    cout<<nazwa;
}
int Gracz::wrece()
{
    return reka.size();
}
int Gracz::prw()
{
    return reka.front();
}
void rozdaj(Karta *k, Gracz *g)
    {
    g->reka.push(k->id);
    k->nalezydo=g->id;
    k->stan=true;
    }
void zagraj(Karta *k, Gracz *g)
    {
    k->stan=false;
    g->reka.pop();
    cout<<"Gracz "<<g->nazwa<<" zagral karte "<<k->wartosc<<" "<<k->kolor<<endl;
    }
void wygraj(Karta *k, Gracz *g)
    {
    if(k->stan==false)
        {
        g->reka.push(k->id);
        k->nalezydo=g->id;
        k->stan=true;
        }
    }
