#include<iostream>
#include <queue>
using namespace std;

class Gracz;
class Karta
{
    string wartosc;
    int starsza;
    string kolor;
    int talia;
    int id;
    int nalezydo; //id gracza, 10 - nierozdana
    bool stan;//0  - lezy na stole

public:
    Karta(int=-1,int=0,int=10,bool=0);
    void nazwij(int,int,int,int);//nazywa karty
    int star();//zwraca starszenstwo karty
    void powiedz();//pokazuje imie karty
      bool czyrozd();//czy karta juz rozdana 0 -nie
    friend void rozdaj(Karta*, Gracz*); //Rozdaje dana karte danemu graczowi
    friend void zagraj(Karta*, Gracz*); //Dany gracz zagrywa dana karte na stol
    friend void wygraj(Karta*, Gracz*); //Dany gracz wygrywa dana karte, jezeli da sie ja wygrac (lezy na stole)


};
class Gracz
{
    string nazwa;
    int id; // od 1 w gore
    int punkty;
    queue<int> reka;
public:
    Gracz(string="Ziomek",int=0);
    void nazwij(string, int);
    bool czygra(); //czy dany gracz gra nadal
    int wrece(); //ile ma kart w rece
    void wolaj(); //imie gracza wyswietla sie na ekranie
    int prw(); //pierwsza karta w rece
    friend void rozdaj(Karta*, Gracz*);
    friend void zagraj(Karta*, Gracz*);
    friend void wygraj(Karta*, Gracz*);
};
