#include<iostream>
#include "karta.h"
#include <queue>
#include <ctime>
#include <cstdlib>
#include<conio.h>
using namespace std;
int main()
{
    Gracz g[9];
    Karta k[52];

    int gracze=0; //ich liczba
    int maxkart; // maksymalna liczba kart dla gracza przy rozdaniu
    cout<<"Gra w wojne!!!!"<<endl;
    srand( time( NULL ) );
    while (gracze<2||gracze>9)
   {
       cout<<"W ilu graczy chcesz grac (2-9)?: ";
        cin>>gracze;
        if (gracze<2||gracze>9) cout<<"Wpisz poprawna wartosc!"<<endl;

   }
    if(52%gracze==0) maxkart=52/gracze;
    else maxkart=52/gracze +1;
    for(int i=1; i<=gracze;i++)
        {
            string imie;
            cout<<"Nazwa gracza "<<i<<": ";
            cin>>imie;
            g[i-1].nazwij(imie,i-1);
        }
    //budowa talii
    for(int i=0;i<52;i++)
    {
        k[i].nazwij(i%13,i%4,1,i);

    }
    //rozdawanie
    int los;
    int cios;
    for(int i=0;i<52;i++)
    {

        do{ los=rand()%gracze;}
        while(g[los].wrece()>=maxkart);

        do{cios=rand()%52;}
        while(k[cios].czyrozd()==true);

            rozdaj(&k[cios],&g[los]);
    }
    bool czykoniec=0; //czy zaszly warunki koncowe
    bool wywolany[9]; //czy powiedziano juz ze gracz przegral
    int tura=1; //ktora tura
    for(int i=0;i<9;i++)wywolany[i]=false;
    //GRA
    while(!czykoniec)
    {
        cout<<"Tura " <<tura<<endl;

        //porownanie kart + wojna
        bool wojna=false; //jezeli prawdziwy, grana jest wojna
        queue<int> boss; //znajda sie tu id graczy, ktorzy wygraja dana ture
        queue<int> warboss; //tymczasowy boss przy wojnie
        int topkarta =-1;//najstarsza karta na stole
        int iletop =1; //ile jest takich kart ze sa najstarsze
        for(int i=0;i<gracze;i++)
        {
            if(g[i].czygra())
            {
                if(k[g[i].prw()].star()==topkarta)
                {
                    iletop++;
                    boss.push(i);
                }
                if(k[g[i].prw()].star()>topkarta)
                {
                    while(!boss.empty())boss.pop();
                    boss.push(i);
                    iletop=1;
                    topkarta=k[g[i].prw()].star();
                }


                zagraj(&k[g[i%gracze].prw()],&g[i%gracze]);// wylozenie kart na stol
            }
        }

            if(iletop>=2)wojna=true;
            while(wojna)
            {
                topkarta=-1;
                iletop=1;
                while(!boss.empty())
                {
                    if(g[boss.front()].wrece()==0)
                    {
                        cout<<"Gracz ";
                        g[boss.front()].wolaj();
                        cout<<" wykrwawil sie podczas wojny i nie ma juz kart!"<<endl;
                        boss.pop();
                    }

                    else
                    {
                        if(g[boss.front()].wrece()==1)
                        {

                            if(k[g[boss.front()].prw()].star()==topkarta)
                            {
                                iletop++;
                                warboss.push(boss.front());

                            }
                            if(k[g[boss.front()].prw()].star()>topkarta)
                            {
                                while(!warboss.empty())warboss.pop();
                                warboss.push(boss.front());
                                iletop=1;
                                topkarta=k[g[boss.front()].prw()].star();
                            }
                            zagraj(&k[g[boss.front()].prw()],&g[boss.front()]);
                            boss.pop();
                        }
                        else
                        {
                             zagraj(&k[g[boss.front()].prw()],&g[boss.front()]);
                             if(k[g[boss.front()].prw()].star()==topkarta)
                            {
                                iletop++;
                                warboss.push(boss.front());

                            }
                            if(k[g[boss.front()].prw()].star()>topkarta)
                            {
                                while(!warboss.empty())warboss.pop();
                                warboss.push(boss.front());
                                iletop=1;
                                topkarta=k[g[boss.front()].prw()].star();
                            }
                            zagraj(&k[g[boss.front()].prw()],&g[boss.front()]);
                            boss.pop();
                        }
                    }
                }
                while(!warboss.empty())
                {
                    boss.push(warboss.front());
                    warboss.pop();
                }
                if(iletop==1)wojna=false;
            }


        //przydzielenie kart zwyciezcy

        for(int i=0;i<52;i++)wygraj(&k[i],&g[boss.front()]);
        //czy ktos juz przegral
        for(int i=0;i<gracze;i++)
        {
            if(!g[i].czygra()&&!wywolany[i])
            {
                cout<<"Gracz ";
                g[i].wolaj();
                cout<<" przegral!!!"<<endl;
                wywolany[i]=true;
            }
        }
        //ile kto ma teraz kart
         for(int i=0;i<gracze;i++)
         {
             if(g[i].czygra())
                {
                cout<<"Gracz ";
                g[i].wolaj();
                cout<<" ma "<<g[i].wrece()<<" kart"<<endl;
                }
         }
        //czy koniec gry
        int lg=0; // liczba wciaz grajacvch
        for(int i=0;i<gracze;i++)
        {
            if(g[i].czygra())lg++;

        }
        if(lg==1)czykoniec=true;
        tura++;
        getch();
    }
    //napisy koncowe
    cout<<"Koniec wojny!!!"<<endl;
    cout<<"Zwyciezca zostaje ";
     for(int i=0;i<gracze;i++)
   {
       if(g[i].czygra())g[i].wolaj();
   }
    return 0;

}
